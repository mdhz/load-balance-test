'use strict'

const express = require('express')
const app = express()
const os = require('os')

app.set('trust proxy', 'loopback, 10.156.0.22')

app.get('/', (req, res, next) => {
  const ifaces = os.networkInterfaces()
  console.log(req.connection)
  const secure = req.secure
  const headers = req.headers
  const hostname = req.hostname
  const protocol = req.protocol
  const ip = req.ip
  const route = req.route
  const remoteAddress = req.connection.remoteAddress
  res.json({
    secure,
    headers,
    hostname,
    protocol,
    ip,
    remoteAddress,
    route,
    ifaces
  })
})

app.use((req, res, next) => {
  let err = new Error('Not Found')
  err.status = 404
  next(err)
})

app.use((err, req, res, next) => {
  return res.status(err.status || 500).json({
    status: err.status,
    message: err.message
  })
})

app.listen(8888, () => {
  console.log(`⚡⚡⚡ WEB SERVER RUNNING AT http://localhost:${8888} ⚡⚡⚡`)
})
